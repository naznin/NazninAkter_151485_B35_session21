<?php

/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 11/2/2016
 * Time: 10:59 AM
 */
//print_r(PDO::getAvailableDrivers());
namespace App\Model;
use PDO;
use PDOException;
class Database
{ public $DBH;
    public $host="localhost";
    public $dbname="atomic_project_B35";
    public $user="root";
    public $pass="";
public function __construct(){

    try {


# MySQL with PDO_MYSQL
        $DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->pass);
        echo "connected successfully";


    }
    catch(PDOException $e){
        echo $e->getMessage();
    }

}
}
$obj= new Database();
# close the connection
$DBH = null;
echo "connection exited";